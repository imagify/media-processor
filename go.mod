module imagify-media-processor

go 1.19

require (
	github.com/disintegration/imaging v1.6.2
	github.com/kolesa-team/go-webp v1.0.2
	go.uber.org/zap v1.24.0
	gocloud.dev v0.28.0
)

require (
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/googleapis/gax-go/v2 v2.7.0 // indirect
	github.com/imkira/go-libav v0.0.0-20190125075901-6bf952df9de5 // indirect
	gitlab.com/imagify/infrastructure-lib v0.0.0-20230217221018-99212b0b0183 // indirect
	gitlab.com/imagify/logging-lib v0.0.0-20230217180742-1361ec69f269 // indirect
	gitlab.com/imagify/storage-lib v0.0.0-20230217182024-a67a20dcc8b7 // indirect
	go.opencensus.io v0.24.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/image v0.3.0 // indirect
	golang.org/x/net v0.4.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect
	google.golang.org/api v0.103.0 // indirect
	google.golang.org/genproto v0.0.0-20221201204527-e3fa12d562f3 // indirect
	google.golang.org/grpc v1.51.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
