package models

const (
	TypeImage = iota
	TypeVideo
	TypeGif
	TypeInvalid
)

type Request struct {
	RequestID string `json:"request_id"`
	FileID    string `json:"file_id"`
	Type      string `json:"type"`
}

func TypeWith(name string) int {
	switch name {
	case "image":
		return TypeImage
	case "video":
		return TypeVideo
	case "gif":
		return TypeGif
	}
	return TypeInvalid
}

type ErrorResponse struct {
	Error string `json:"error"`
}
