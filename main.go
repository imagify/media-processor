package main

import (
	"context"
	"encoding/json"
	"flag"
	"gitlab.com/imagify/infrastructure-lib"
	logging "gitlab.com/imagify/logging-lib"
	storage "gitlab.com/imagify/storage-lib"
	"go.uber.org/zap"
	"gocloud.dev/pubsub"
	"imagify-media-processor/models"
	"imagify-media-processor/runtime"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	concurrency := parseArgs()
	log := logging.NewLogger(infrastructure.IsProduction())
	store := storage.New(infrastructure.Services().Storage())
	ctx, _cancel := context.WithCancel(context.Background())
	cancel := func(message string) {
		log.Info("Shutting down context", zap.String("message", message))
		_cancel()
	}
	executor := runtime.Start(concurrency, store, log, ctx, cancel)
	go func() {
		sub, err := pubsub.OpenSubscription(ctx, infrastructure.Kafka().MediaProcessorReadTopic())
		if err != nil {
			cancel("Cannot open subscription")
			return
		}
		defer func() { _ = sub.Shutdown(ctx) }()
		for {
			msg, err := sub.Receive(ctx)
			if err != nil {
				cancel("Cannot receive message")
				return
			}
			msg.Ack()
			body, err := decodeMessageBody(msg)
			if err != nil {
				log.Warn("Cannot decode message, ack and skip")
				continue
			}
			executor.In <- body
		}
	}()
	go func() {
		top, err := pubsub.OpenTopic(ctx, infrastructure.Kafka().MediaProcessorWriteTopic())
		if err != nil {
			cancel("Cannot open topic")
			return
		}
		defer func() { _ = top.Shutdown(ctx) }()
		for done := range executor.Out {
			var message []byte
			if done.Done != nil {
				message, _ = json.Marshal(done.Done)
			} else {
				message, _ = json.Marshal(done.Fail)
			}
			if err := top.Send(ctx, &pubsub.Message{Body: message}); err != nil {
				cancel("Cannot send to topic")
				return
			}
		}
	}()
	sign := make(chan os.Signal)
	signal.Notify(sign, syscall.SIGINT)
	<-sign
	cancel("SIGINT caught")
}

func parseArgs() (concurrency int) {
	config := flag.String(
		"config", "",
		"Path to infrastructure config",
	)
	flag.IntVar(
		&concurrency, "concurrency", 32,
		"Number of goroutines that processing tasks (from 1 to 1024)",
	)
	flag.Parse()
	if concurrency < 1 || concurrency > 1024 {
		panic("Invalid concurrency value (_ > 1 && _ < 1024)")
	}
	if err := infrastructure.Load(*config); err != nil {
		panic("Invalid path to config")
	}
	return
}

func decodeMessageBody(msg *pubsub.Message) (*models.Request, error) {
	raw := new(models.Request)
	return raw, json.Unmarshal(msg.Body, raw)
}
