package runtime

import (
	"context"
	"errors"
	"fmt"
	"github.com/disintegration/imaging"
	"github.com/kolesa-team/go-webp/encoder"
	storage "gitlab.com/imagify/storage-lib"
	"go.uber.org/zap"
	"image"
	"imagify-media-processor/models"
	"io"
	"os"
	"os/exec"
	"strings"
)

const (
	ErrorCanceled = iota
	ErrorInternal
	ErrorConvert
)

type Data struct {
	FilePath   string
	FramePaths []string
}

func imageData(path string) *Data {
	return &Data{
		FilePath:   path,
		FramePaths: nil,
	}
}

func videoData(path string, frames []string) *Data {
	return &Data{
		FilePath:   path,
		FramePaths: frames,
	}
}

type Error struct {
	inner error
	ttype int
}

func (e *Error) JSON() []byte {
	return []byte(fmt.Sprintf(
		`{"error":{""}}`,
	))
}

func (e *Error) Type() int {
	return e.ttype
}

func (e *Error) Error() string {
	return e.inner.Error()
}

func (e *Error) Is(target error) bool {
	return e.ttype == ErrorCanceled && errors.Is(target, context.Canceled)
}

func (e *Error) Unwrap() error {
	return e.inner
}

func canceledError() *Error {
	return &Error{nil, ErrorCanceled}
}

func internalError(err error) *Error {
	return &Error{err, ErrorInternal}
}

func convertError(err error) *Error {
	return &Error{err, ErrorConvert}
}

type CompletionContext struct {
	Done *DoneContext
	Fail *Error
}

type DoneContext struct {
	OutputFile   string
	OutputFrames []string
}

type Executor struct {
	In     chan *models.Request
	Out    chan *CompletionContext
	store  *storage.Store
	log    *zap.Logger
	ctx    context.Context
	cancel func(string)
}

func Start(
	concurrency int,
	store *storage.Store,
	log *zap.Logger,
	ctx context.Context,
	cancel func(string),
) *Executor {
	e := &Executor{
		In:     make(chan *models.Request),
		Out:    make(chan *CompletionContext),
		store:  store,
		log:    log,
		ctx:    ctx,
		cancel: cancel,
	}
	for i := 0; i < concurrency; i++ {
		go func() {
			for request := range e.In {
				e.handleRequest(request)
			}
		}()
	}
	return e
}

func (e *Executor) handleRequest(request *models.Request) {
	download, err := e.store.Download(request.FileID, e.ctx)
	if err != nil {
		if !errors.Is(err, context.Canceled) {
			e.cancel("Cannot download file")
		}
		return
	}
	defer func() {
		_ = download.Close()
		_ = os.Remove(download.Name())
	}()
	var done *Data
	var doError *Error
	switch models.TypeWith(request.Type) {
	case models.TypeImage:
		done, doError = e.doImage(download)
	case models.TypeVideo:
		done, doError = e.doVideo(download)
	case models.TypeGif:
		done, doError = e.doGif(download)
	}
	if doError != nil {
		if !errors.Is(err, context.Canceled) {
			e.Out <- &CompletionContext{Fail: doError}
		}
	} else {
		open, _ := os.Open(done.FilePath)
		defer func() {
			_ = open.Close()
			_ = os.Remove(open.Name())
		}()
		fileID, err := e.store.Upload(open, e.ctx)
		if err != nil {
			if !errors.Is(err, context.Canceled) {
				e.cancel("Cannot upload file")
			}
			return
		}
		var frameIds []string
		var files []*os.File
		defer func() {
			for _, file := range files {
				_ = file.Close()
				_ = os.Remove(file.Name())
			}
		}()
		for _, frame := range done.FramePaths {
			open, _ := os.Open(frame)
			files = append(files, open)
			id, err := e.store.Upload(open, e.ctx)
			if err != nil {
				if !errors.Is(err, context.Canceled) {
					e.cancel("Cannot upload file")
				}
				return
			}
			frameIds = append(frameIds, id)
		}
		e.Out <- &CompletionContext{Done: &DoneContext{fileID, frameIds}}
	}
}

func (e *Executor) doImage(file *os.File) (*Data, *Error) {
	img, err := imaging.Decode(file)
	if err != nil {
		fmt.Println(err)
		return nil, convertError(err)
	}
	temp, err := os.CreateTemp("", "*.webp")
	if err != nil {
		e.cancel("Cannot create temp file")
		return nil, canceledError()
	}
	if err := encodeImage(img, temp); err != nil {
		_ = temp.Close()
		_ = os.Remove(temp.Name())
		return nil, internalError(err)
	}
	return imageData(temp.Name()), nil
}

func encodeImage(img image.Image, to io.Writer) error {
	options, err := encoder.NewLossyEncoderOptions(encoder.PresetDefault, 100)
	if err != nil {
		return err
	}
	enc, err := encoder.NewEncoder(img, options)
	if err != nil {
		return err
	}
	if err := enc.Encode(to); err != nil {
		return err
	}
	return nil
}

func (e *Executor) doGif(file *os.File) (*Data, *Error) {
	_ = file.Close()
	temp, err := os.CreateTemp("", "*.webm")
	if err != nil {
		e.cancel("Cannot create temp file")
		return nil, canceledError()
	}
	_ = temp.Close()
	cmd := exec.CommandContext(
		e.ctx,
		"bash",
		"-c",
		strings.Join([]string{
			"ffmpeg",
			"-i",
			file.Name(),
			"-y",
			"-crf",
			"30",
			temp.Name(),
			"-an",
			"-noautorotate",
			"-preset",
			"ultrafast",
			"-cpu-used",
			"2",
			"-c:v libvpx-vp9",
			"-threads",
			"8",
		}, " "),
	)
	err = cmd.Start()
	if err != nil {
		_ = os.Remove(temp.Name())
		e.cancel("Error while starting subprocess: " + err.Error())
		return nil, canceledError()
	}
	done := make(chan error)
	go func() { done <- cmd.Wait() }()
	select {
	case <-e.ctx.Done():
		_ = os.Remove(temp.Name())
		return nil, canceledError()
	case err := <-done:
		if err != nil {
			e.log.Warn("Subprocess failed", zap.Error(err))
			_ = os.Remove(temp.Name())
			return nil, convertError(err)
		}
	}
	return videoData(temp.Name(), nil), nil
}

func (e *Executor) doVideo(file *os.File) (*Data, *Error) {
	_ = file.Close()
	temp, err := os.CreateTemp("", "*.webm")
	if err != nil {
		e.cancel("Cannot create temp file")
		return nil, canceledError()
	}
	_ = temp.Close()
	cmd := exec.CommandContext(
		e.ctx,
		"bash",
		"-c",
		strings.Join([]string{
			"ffmpeg",
			"-i",
			file.Name(),
			"-y",
			"-crf",
			"30",
			temp.Name(),
			"-noautorotate",
			"-preset",
			"ultrafast",
			"-cpu-used",
			"2",
			"-c:v libvpx-vp9",
			"-threads",
			"8",
		}, " "),
	)
	err = cmd.Start()
	if err != nil {
		_ = os.Remove(temp.Name())
		e.cancel("Error while starting subprocess: " + err.Error())
		return nil, canceledError()
	}
	done := make(chan error)
	go func() { done <- cmd.Wait() }()
	select {
	case <-e.ctx.Done():
		_ = os.Remove(temp.Name())
		return nil, canceledError()
	case err := <-done:
		if err != nil {
			e.log.Warn("Subprocess failed", zap.Error(err))
			_ = os.Remove(temp.Name())
			return nil, convertError(err)
		}
	}
	return videoData(temp.Name(), nil), nil
}
